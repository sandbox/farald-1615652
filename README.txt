This module creates a new display plugin for views, called Jvectormap.
It is a lightweight way to make a clickable worldmap, where the entire country is clickable.
You can also change the color of each country on the map.

Master is empty. Please check the 7.x-1.x branch for the code.
